#!/bin/bash
ENV_FILE=/etc/environment
cat <<EOF >>$ENV_FILE
LANG=en_US.utf-8
LC_ALL=en_US.utf-8
EOF

#install Java and Maven
sudo apt-get update -y
sudo apt-get install openjdk-8-jdk -y
sudo apt-get install maven -y
#install CodeDeploy-agent
apt-get install python-pip -y
sudo pip install awscli
sudo apt-get install software-properties-common python-software-properties -y
sudo add-apt-repository ppa:brightbox/ruby-ng-experimental -y
sudo apt-get update -y
sudo apt-get install ruby2.0 -y
sudo apt-get install wget -y
cd
wget https://aws-codedeploy-us-west-2.s3.amazonaws.com/latest/install
chmod +x ./install
sudo ./install auto
sudo apt-get install -y unzip
sudo service codedeploy-agent start


#install CloudWatch logging service
curl https://s3.amazonaws.com//aws-cloudwatch/downloads/latest/awslogs-agent-setup.py -O
chmod +x ./awslogs-agent-setup.py
./awslogs-agent-setup.py -n -r us-west-2 -c s3://aws-codedeploy-us-east-1/cloudwatch/awslogs.conf
#Include CodeDeploy logs config
sudo mkdir -p /var/awslogs/etc/config
wget https://aws-codedeploy-us-west-2.s3.amazonaws.com/cloudwatch/codedeploy_logs.conf
cp ./codedeploy_logs.conf /var/awslogs/etc/config/codedeploy_logs.conf
sudo systemctl enable awslogs
sudo systemctl start awslogs