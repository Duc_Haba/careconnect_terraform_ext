// Step 1

provider "aws" {
  region      = "${var.region}"
  profile     = "${var.profile}"
}

//Step 2

resource "aws_route53_record" "careconnect-backend-r53" {
  zone_id = "ZGPZXFJHFRT0"
  name    = "careconnect-api.ymedia.in"
  type    = "A"
  alias {
    name                   = "yml-internal-alb-1103207077.us-west-2.elb.amazonaws.com"
    zone_id                = "Z1H1FL5HABSF5"
    evaluate_target_health = false
  }
}

// Step 3

resource "aws_lb_target_group" "careconnect-backend-dev" {
  name     = "careconnect-backend-dev-tg"
  port     = 9000
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
}

//  Create a listener rule

resource "aws_alb_listener_rule" "careconnect-backend-dev-listener-rule-www" {
  listener_arn = "${var.listener_arn}"
  
  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.careconnect-backend-dev.id}"
  }
  condition {
    field  = "host-header"
    values = ["careconnect-api.ymedia.in"]
  }
}

// Step 4

data "aws_iam_policy_document" "dev-instance-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "dev-instance-role" {
  name               = "CareconnectBackendInstanceRole"
  path               = "/system/"
  assume_role_policy = "${data.aws_iam_policy_document.dev-instance-assume-role-policy.json}"
}

resource "aws_iam_instance_profile" "dev-instance-profile" {
  name  = "CareconnectBackendInstanceProfile"
  role = "${aws_iam_role.dev-instance-role.name}"
}

// Step 5

resource "aws_codedeploy_app" "careconnect-backend-cd-app" {
  name = "careconnect-backend"
}

data "aws_iam_policy_document" "codedeploy-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["codedeploy.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "careconnect_backend_codedeploy_service_role" {
  name = "CareconnectBackendCodeDeployServiceRole"
  assume_role_policy = "${data.aws_iam_policy_document.codedeploy-assume-role-policy.json}"
}

resource "aws_codedeploy_deployment_group" "careconnect-backend-cd-dg-dev" {
  deployment_group_name = "dev"
  app_name              = "${aws_codedeploy_app.careconnect-backend-cd-app.name}"
  service_role_arn      = "${aws_iam_role.careconnect_backend_codedeploy_service_role.arn}"
  deployment_config_name = "CodeDeployDefault.OneAtATime"
    // Create a tag filter that will identify the ec2-instance that will be associated.
  ec2_tag_filter {
    type  = "KEY_AND_VALUE"
    key   = "careconnect-backend-server-version"
    value = "v1"
  }
}

resource "aws_iam_role_policy_attachment" "codedeploy_service_role" {

  role      = "${aws_iam_role.careconnect_backend_codedeploy_service_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole"
}

    // Attach the policy to instance role in Step 2
resource "aws_iam_role_policy_attachment" "codedeploy-policy-attachment" {
    role       = "${aws_iam_role.dev-instance-role.name}"
    policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforAWSCodeDeploy"
}

// Step 6
module "careconnect-backend-ec2" {
  source = "git::ssh://git@bitbucket.org/ymedialabs/terraform-modules.git//modules//ec2?ref=v0.42"
  subnet_id = "${var.subnet_id}"
  vpc_id = "${var.vpc_id}"
  instance_name         = "Careconnect-API-Server"
  ami_id                = "${var.careconnect_backend_ami_id}"
  instance_type         = "t2.micro"
  key_name              = "careconnect_API_kp"
  allow_inbound_ips     = [ "22, 0.0.0.0/0" ]
  inbound_sgs_count     = 1
  allow_inbound_sgs     = [ "9000, ${var.alb_sg_id}" ]
  volume_size           = 8
  iam_instance_profile  = "${aws_iam_instance_profile.dev-instance-profile.name}"
  additional_tags {
    for = "Shweta"
    company = "yml"
    project = "careconnect"
    managed-by = "Terraform"
    careconnect-backend-server-version = "v1"
  }
  user_data = "${file("dev-user-data.sh")}"
}