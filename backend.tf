terraform {
  backend "s3" {
    bucket = "yml-terraform-state-storage"
    key    = "careconnect-backend/dev/terraform.tfstate"
    region = "us-west-2"
    profile = "yml"
  }
}